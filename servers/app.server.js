const express = require('express');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
const jwt = require ('jsonwebtoken');

const authdb = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/users.json')));

const SECRET_KEY = "rhlbz-secret-key"
const expiresIn = '1h'

// create and open server
const app = express();
const port = process.env.PORT || 8080;
app.use(express.static('./'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../index.html'));
});
app.listen(port);

////////////////////////////////////////////////////////////////////

// features funcs
const randomID = () => {
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const length = 10;
    let randomStr = "";
    for (let i = 0; i < length; i++) {
        const randomNum = Math.floor(Math.random() * characters.length);
        randomStr += characters[randomNum];
    }
    return randomStr;
}
function verifyToken(token){
    return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}
function isNotAuthenticated({email, password}){
    return authdb.users.findIndex(user => user.email === email && user.password === password) !== -1
}
function getUserInfo(id){
    if ( id ){
        return authdb.users.find(user => user.id === id )
    }
}
function getUserId({email, password}){
    if ( email && password ){
        return authdb.users.find(user => { if (user.email === email && user.password === password) return user.id; } )
    } else {
        return 406
    }
}
const validatePassword = password => {
    if ( password.length < 5 ){
        return false;
    } else {
        var regex = new RegExp(/^(?=.*[a-zA-Z])(?=.*[0-9])/, "g");
        return regex.test(password)
    }
} 
const validateString = string => {
    if ( string.length == 0 ){
        return false;
    } else {
        var regex = new RegExp(/^(?=.*[a-zA-Z])/, "g");
        return regex.test(string)
    }
} 
const validateDigit = digit => {
    if ( digit.length == 0 ){
        return false;
    } else {
        var regex = new RegExp(/^(?=.*[0-9])/, "g");
        return regex.test(digit)
    }
} 
const validateEmail = email => {
    var regex = new RegExp(/@/, "g");
    console.log("PASSWORD.TEST ", regex.test(email))
    return regex.test(email)
} 
const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, SECRET_KEY, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

///////////////////////////////////////////////////////////////////////
// apis
// Register New User
app.post('/auth/register', (req, res) => {
    const {email, password} = req.body;
    if ( email.length == 0 || password.lentgh == 0 ){
        const status = 406;
        const message = 'Riempi tutti i campi';  
        return res.status(status).send(message);
    }
    let valid_password = validatePassword(password);
    if ( !valid_password ){
        const status = 406;
        const message = 'La password non rispetta i canoni';  
        return res.status(status).send(message);
    }
    if( isNotAuthenticated({email, password}) ) {
        const status = 401;
        const message = 'Queste credenziali esistono già';  
        return res.status(status).send(message);
    } else {
        fs.readFile((path.join(__dirname, '../data/users.json')) , (err, data) => {  
            if (err) {
                const status = 401
                const message = err;                
                return res.status(status).send(message);
            } else {
                var data = JSON.parse(data.toString());
                const newid = randomID();
                const new_user = {
                    id: newid, 
                    email: email, 
                    password: password,
                    name: "",
                    surname: "",
                    phone: ""
                }
                data.users.push(new_user); 
                fs.writeFile((path.join(__dirname, '../data/users.json')) , JSON.stringify(data), (err, result) => {
                    if (err) {
                        const status = 401
                        const message = err
                        return res.status(status).send(message);
                    } else {
                        const accessToken = jwt.sign({ username: new_user.email,  password: new_user.password }, SECRET_KEY);
                        const usr = {
                            id: newid,
                            access_token: accessToken
                        }
                        return res.send(200, usr);
                    }
                });
            }
        });
    }
})
// Base info 
app.put('/users/base-info/:id', authenticateJWT, (req, res) => {
    const {id} = req.params;
    const {name, surname} = req.body;
    if ( !id ){
        const status = 404;
        const message = 'Credenziali non trovate';  
        return res.status(status).send(message);
    }
    if ( name.length == 0 || surname.length == 0 ){
        const status = 406;
        const message = 'Riempi tutti i campi';  
        return res.status(status).send(message);
    }
    let valid_name = validateString(name);
    let valid_surname = validateString(surname);
    if ( !valid_name || !valid_surname ){
        const status = 406;
        const message = 'Si accettano solo caratteri alfabetici';  
        return res.status(status).send(message);
    }
    const userInfo = getUserInfo(id);
    if ( !userInfo || userInfo === undefined || userInfo === null ){
        const status = 404;
        const message = 'Credenziali non trovate';  
        return res.status(status).send(message);
    } else {
        fs.readFile((path.join(__dirname, '../data/users.json')) , (err, data) => {  
            if (err) {
                const status = 401
                const message = err;                
                return res.status(status).send(message);
            } else {
                
                var data = JSON.parse(data.toString());
                userInfo.name = name;
                userInfo.surname = surname;
                data.users.map( user => {
                    if ( user.id === userInfo.id ){
                        user.name = userInfo.name;
                        user.surname = userInfo.surname;
                    }
                });

                fs.writeFile((path.join(__dirname, '../data/users.json')) , JSON.stringify(data), (err, result) => { 
                    if (err) {
                        const status = 401
                        const message = err
                        return res.status(status).send(message);
                    } else {
                        const id = userInfo.id;
                        return res.send(200, id);
                    }
                });
            }
        });
    }
})
// Contact info
app.put('/users/contact-info/:id', authenticateJWT, (req, res) => {
    const {id} = req.params;
    const {phone} = req.body;
    if ( !id ){
        const status = 404;
        const message = 'Credenziali non trovate';  
        return res.status(status).send(message);
    }
    if ( phone.length == 0 ){
        const status = 406;
        const message = 'Riempi tutti i campi';  
        return res.status(status).send(message);
    }
    let valid_digit = validateDigit(phone);
    if ( !valid_digit ){
        const status = 406;
        const message = 'Il numero di telefono può contenere solo numeri';  
        return res.status(status).send(message);
    }
    const userInfo = getUserInfo(id);
    if ( !userInfo || userInfo === undefined || userInfo === null ){
        const status = 404;
        const message = 'Credenziali non trovate';  
        return res.status(status).send(message);
    } else {
        fs.readFile((path.join(__dirname, '../data/users.json')) , (err, data) => {  
            if (err) {
                const status = 401
                const message = err;                
                return res.status(status).send(message);
            } else {
                
                var data = JSON.parse(data.toString());
                userInfo.phone = parseInt(phone);
                data.users.map( user => {
                    if ( user.id === userInfo.id ){
                        user.phone = userInfo.phone;
                    }
                });

                fs.writeFile((path.join(__dirname, '../data/users.json')) , JSON.stringify(data), (err, result) => {
                    if (err) {
                        const status = 401
                        const message = err
                        return res.status(status).send(message);
                    } else {
                        const id = userInfo.id;
                        return res.send(200, id);
                    }
                });
            }
        });
    }
})
// User info
app.get('/users/user/:id', authenticateJWT, (req, res) => {
    const {id} = req.params;
    if ( !id ){
        const status = 404;
        const message = 'Account non trovato';  
        return res.status(status).send(message);
    }
    const userInfo = getUserInfo(id);
    if ( !userInfo || userInfo === undefined || userInfo === null ){
        const status = 404;
        const message = 'Account non trovato';  
        return res.status(status).send(message);
    } else {
        const status = 200
        const usr = {
            id: userInfo.id,
            email: userInfo.email,
            name: userInfo.name,
            surname: userInfo.surname,
            phone: userInfo.phone
        }
        return res.send(200, usr);
    }
})
// Login to one of the users from ./users.json
app.post('/auth/login', (req, res) => {
    const {email, password} = req.body;
    if (isNotAuthenticated({email, password}) === false) {
        const message = "Le credenziali che hai inserito non sono state registrate";
        res.send(404, message);
        return res;
    } else {
        //const access_token = createToken({email, password})
        const accessToken = jwt.sign({ username: email,  password: password }, SECRET_KEY);
        const user = getUserId({email, password})
        if ( user != 406 ){
            const usr = {
                id: user.id,
                access_token: accessToken
            }
            return res.send(200, usr);
        } else {
            const message = 'Non sono stati inviati i dati richiesti';  
            return res.send(406, message);
        }
    }
})