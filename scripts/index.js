// require('../servers/db.connection');
// require('../servers/db.services');
let BASE_URL = window.location.origin;

function mobileCheck() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
}

let currentTab = 0;

$(document).ready(function(){

    
    $("#logga").click(function(){
        loginSection();
    });
    $("#signup").click(function(){
        signupSection();
    });
    $("#loginBtn").click(function(){
        login();
    });

    showTab(currentTab);

});

function loginSection(){
    $("#logga").removeClass("show");
    $("#logga").addClass("hide");
    $("#signup").removeClass("hide");
    $("#signup").addClass("show");
    $("#signup--section").removeClass("show");
    $("#signup--section").addClass("hide");
    $("#login--section").removeClass("hide");
    $("#login--section").addClass("show");
}

function signupSection(){
    $("#signup").removeClass("show");
    $("#signup").addClass("hide");
    $("#logga").removeClass("hide");
    $("#logga").addClass("show");
    $("#login--section").removeClass("show");
    $("#login--section").addClass("hide");
    $("#signup--section").removeClass("hide");
    $("#signup--section").addClass("show");
}

function showTab(n) {
    // This function will display the specified tab of the form...
    var tabs = document.getElementsByClassName("tab");
    if ( tabs[n] ){
        tabs[n].classList.add("show")
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n);
    disableTab(n+1)
}

function nextPrev() {
    var tab = document.getElementsByClassName("moving--next");
    if ( currentTab ==  0 ){
        let email = $("#signup--email").val();
        let password = $("#signup--password").val();
        $("#signup--email").addClass("current-input");
        $("#signup--password").addClass("current-input");
        let user = { email: email, password: password }
        createCredential(user, tab)
    } else if ( currentTab == 1 ){
        let name = $("#signup--name").val();
        let surname = $("#signup--surname").val();
        $("#signup--name").addClass("current-input");
        $("#signup--surname").addClass("current-input");
        let user = { name: name, surname: surname }
        updateUserBaseInfo(user, tab)
    } else if ( currentTab == 2 ){
        let phone = $("#signup--phone").val();
        $("#signup--phone").addClass("current-input");
        let user = { phone: phone }
        updateUserContactInfo(user, tab);
    } 
}


function fixStepIndicator(n) {
    var t = document.getElementsByClassName("tab")
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" step-active", " step-passed");
        t[i].className = t[i].className.replace(" show moving--next", " hide moving--prev");
    }
    if ( x[n] ){
        x[n].className += " step-active";
        t[n].className += " moving--next";

    }
}

function login(){
    let email = $("#login--email").val();
    let password = $("#login--password").val();
    $("#login--email").addClass("current-input");
    $("#login--password").addClass("current-input");
    let user = { email: email, password: password }

    $.ajax({
        type: 'POST',
        url: BASE_URL + '/auth/login',
        data: user,
        beforeSend: function(){
            $("#login--form").addClass("blur");
            $("#logga").removeClass("show");
            $("#logga").addClass("hide");
            $("#signup").removeClass("show");
            $("#signup").addClass("hide");
        },
        statusCode: {
            200: function(res){
                window.sessionStorage.setItem('usrId', res.id)
                getUserInfo()
            }, 
            401: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'login--error',
                    msg: msg,
                }
                $("#login--form").removeClass("blur");
                generateError(data);
            },
            403: function(res) {
                let msg = res;
                let data = {
                    id: 'login--error',
                    msg: msg,
                }
                $("#login--form").removeClass("blur");
                generateError(data);
            },
            404: function(res) {
                let msg = res;
                let data = {
                    id: 'login--error',
                    msg: msg,
                }
                $("#login--form").removeClass("blur");
                generateError(data);
            },
            406: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'login--error',
                    msg: msg,
                }
                $("#login--form").removeClass("blur");
                generateError(data);
            },
            500: function(res) {
                let msg = "Ops... abbiamo riscontrato un problema che non dipende te";
                let data = {
                    id: 'login--error',
                    msg: msg,
                }
                $("#login--form").removeClass("blur");
                generateError(data);
            }
        },
        success: function(res){
        }
    })
}
function createCredential(user, tab){
    $.ajax({
        type: 'POST',
        url: BASE_URL + '/auth/register',
        data: user,
        statusCode: {
            200: function(res){
                window.sessionStorage.setItem('usrId', res.id);
                window.sessionStorage.setItem('tkn', res.access_token);
                currentTab = currentTab + 1;
                showTab(currentTab);
            },
            401: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'mail-password--error',
                    msg: msg,
                }
                generateError(data);
            },
            406: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'mail-password--error',
                    msg: msg,
                }
                generateError(data);
            },
            500: function(res) {
                let msg = "Ops... abbiamo riscontrato un problema che non dipende te";
                let data = {
                    id: 'mail-password--error',
                    msg: msg,
                }
                generateError(data);
            }
        },
        success: function(res){
        }
    })
}
function updateUserBaseInfo(user, tab){
    const usrId = window.sessionStorage.getItem("usrId");
    console.log(" usrID ", usrId)
    const tkn = window.sessionStorage.getItem('tkn');
    $.ajax({
        type: 'PUT',
        url: BASE_URL + '/users/base-info/'+ usrId,
        data: user,
        headers:{
            "Authorization": "Bearer " + tkn + ""
        },
        statusCode: {
            200: function(res){
                currentTab = currentTab + 1;
                showTab(currentTab);
            },
            401: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'name-surname--error',
                    msg: msg,
                }
                generateError(data);
            },
            406: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'name-surname--error',
                    msg: msg,
                }
                generateError(data);
            },
            500: function(res) {
                let msg = "Ops... abbiamo riscontrato un problema che non dipende te";
                let data = {
                    id: 'name-surname--error',
                    msg: msg,
                }
                generateError(data);
            }
        },
        success: function(res){
        }
    })
}
function updateUserContactInfo(user, tab){
    const usrId = window.sessionStorage.getItem("usrId");
    const tkn = window.sessionStorage.getItem('tkn');
    $.ajax({
        type: 'PUT',
        url: BASE_URL + '/users/contact-info/'+ usrId,
        data: user,
        headers:{
            "Authorization": "Bearer " + tkn + ""
        },
        statusCode: {
            200: function(res){
                $("#signup--form").addClass("blur");
                $("#logga").removeClass("show");
                $("#logga").addClass("hide");
                $("#signup").removeClass("show");
                $("#signup").addClass("hide");
                getUserInfo();
            },
            401: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'phone--error',
                    msg: msg,
                }
                generateError(data);
            },
            406: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'phone--error',
                    msg: msg,
                }
                generateError(data);
            },
            500: function(res) {
                let msg = "Ops... abbiamo riscontrato un problema che non dipende da te";
                let data = {
                    id: 'phone--error',
                    msg: msg,
                }
                generateError(data);
            },
        },
        success: function(res){
        }
    })
}
function getUserInfo(){
    const usrId = window.sessionStorage.getItem('usrId');
    const tkn = window.sessionStorage.getItem('tkn');
    $.ajax({
        type: 'GET',
        headers:{
            "Authorization": "Bearer " + tkn + ""
        },
        url: BASE_URL + '/users/user/'+ usrId,
        beforeSend: function() {   
            $("#signup--form").addClass("blur");
            $("#logga").removeClass("show");
            $("#logga").addClass("hide");
            $("#signup").removeClass("show");
            $("#signup").addClass("hide");
        },
        statusCode: {
            200: function(res){
                $(".end--user-name").each(function(){
                    $(this).text(res.name);
                });
                $("#end--user-surname").text(res.surname);
                $("#end--user-email").text(res.email);
                $("#end--user-phone").text(res.phone);
                
                $("#logga").addClass("hide");
                $("#signup").addClass("hide");
                $("#signup--section").removeClass("show");
                $("#signup--section").addClass("hide");
                $("#login--section").removeClass("show");
                $("#login--section").addClass("hide");
                $("#users-data--section").removeClass("hide");
                $("#users-data--section").addClass("show");
            },
            401: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'phone--error',
                    msg: msg,
                }
                generateError(data);
                return false;
            },
            406: function(res) {
                let msg = res.responseText;
                let data = {
                    id: 'phone--error',
                    msg: msg,
                }
                generateError(data);
            },
            500: function(res){
                let msg = "Ops... abbiamo riscontrato un problema che non dipende da te";
                let data = {
                    id: 'phone--error',
                    msg: msg,
                }
                generateError(data);
            }
        },
        success: function(res){
        }
    })
}

function removeErrorClassFromField(){
    $(".current-input").each( function() {
        if ( $(this).hasClass("error-in-fields") ){
            $(this).removeClass("error-in-fields");
        }
        hideErrorMessage()
    })
}

function hideErrorMessage(){
    $(".error--fields").each( function() {
        if ( $(this).hasClass("visible") ){
            $(this).removeClass("visible");
            $(this).addClass("no-visible");
        }
    });
}

function generateError(data){
    $("#"+data.id).text(data.msg);
    $("#"+data.id+"-container").removeClass("no-visible");
    $("#"+data.id+"-container").addClass("visible");
    $(".current-input").addClass("error-in-fields");
    $(".current-input").on('input', function(){
        removeErrorClassFromField();
    })
}

function disableTab(step){
    switch(step){
        case 1:{
            $("#signup--email").removeAttr("disabled");
            $("#signup--password").removeAttr("disabled");
            $("#signup--name").attr("disabled", "disabled");
            $("#signup--surname").attr("disabled", "disabled");
            $("#signup--phone").attr("disabled", "disabled");
            $("#email-password--btn").css("display", "block");
            $("#name-surname--btn").css("display", "none");
            $("#phone--btn").css("display", "none");
        }
        break;
        case 2:{
            $("#signup--name").removeAttr("disabled");
            $("#signup--surname").removeAttr("disabled");
            $("#signup--email").attr("disabled", "disabled");
            $("#signup--password").attr("disabled", "disabled");
            $("#signup--phone").attr("disabled", "disabled");
            $("#email-password--btn").css("display", "none");
            $("#name-surname--btn").css("display", "block");;
            $("#phone--btn").css("display", "none");
        }
        break;
        case 3:{
            $("#signup--email").attr("disabled", "disabled");
            $("#signup--password").attr("disabled", "disabled");
            $("#signup--name").attr("disabled", "disabled");
            $("#signup--surname").attr("disabled", "disabled");
            $("#signup--phone").removeAttr("disabled");
            $("#email-password--btn").css("display", "none");
            $("#name-surname--btn").css("display", "none");
            $("#phone--btn").css("display", "block");
        }
        break;
        case deafult: {
            return;
        }

    }
}